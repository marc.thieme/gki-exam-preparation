#import "@local/lecture-notes-template:0.0.0": *
#import "@local/theorems:0.0.0": *

#let (theorem, proposition, definition, todo, remark, corollary, example, lemma, notation) = init-theorems("theorems", 
  theorem: ("Satz",),
  proposition: ("Proposition",),
  definition: ("Definition",),
  todo: ("TODO",),
  remark: ("Remark",),
  corollary: ("Korollar",),
  example: ("Beispiel",),
  lemma: ("Lemma",),
  notation: ("Notation",),
)

