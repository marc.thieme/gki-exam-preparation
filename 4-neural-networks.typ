#import "lib.typ": *

= Lecture 4 | Neuronale Netze
== Activation Functions
+ Sigmoid
+ ReLU
+ tanh
+ Leaky ReLU
+ Maxout
+ ELU

#lemma("Netztiefe abwägung")[
$->$ Netze mit einem Layer können prinzipell alle Funktionen approximieren
- Tiefere Netze brauchen weniger Neuronen, _aber_
- Tiefere Netze sind eventuell schwerer zu optimieren/trainieren
]
#notation("Computational Graph")[
  Erinnere an autograd von Pytorch. Ermöglicht modulare/rekursive Berechnung des Gradients für beliebige Parameter
]
#theorem("Komplexität backpropagation")[
  - Kosten sind linear in der Anzahl der Schichten
  - Kosten sind quadratisch in der Anzahl der Neuronen pro Schicht
]

#corollary("Problems with gradients")[
/ Vanishing gradients: Gradients stray towards zero and the parameter updates don't change anything anymore
/ Exploding gradients: Gradients occilate wildy
]

#proposition("Gradient Descent Tricks")[
/ Momnentum: Speichere Momentum für Parameter. Veränder Wert d. Parameters bzgl. Momentum.
   Gradient verändert nur das Momentum und nicht direkt den Parameter selbst.
/ Gradient normalization: Skaliere Parameter Update in terms of the value of the previous gradient.
   Leads to a larger scaling of the parameter update when the gradient is steep for multiple generations.
/ Adam (Adaptive Momentum): Kombiniere Momentum und Gradient Normalization
/ Leraning rate decay: Passe Lernrate nach $k$ Epochen an
]

