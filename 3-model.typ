#import "lib.typ": *

= Lecture 3 | Modellierung
== Begriffe
#quote[A computer program is said to learn from *experience E* with respect to some
  class of *tasks T* and *performance measure P*, if its performance at tasks in
  T, as measured by P, improves with experience E.]
#example("Tasks")[
  - Klassifikation
  - Regression
  - Synthese
  - Anomaly Detection
  - Transkription
    - Unstrukturierter Input zu Text
  - Translation
]
/ Performance Measure: can be an error metric
/ Experience E: A Dataset

#remark[
  Unterschied zwischen ML und Optimierung ist unter anderem, dass bei ML eine
  Generalisierung stattfindet. Hierbei kommt es zu _Generalisierungsfehler_.
]

*Overfitting* $<-->$ *Underfitting*
#definition(
  "Kapazität",
)[
  Die Möglichkeit eines Modells, viele unterschiedliche Funktionen zu beschreiben
]

== Warum Mean-Squared-Error?
_Gegeben_: $m$ Datenpunkt $X = {x_1, ..., x_m} tilde p_"data" (x)$, wobei $p_"data"$ eine
unbekannte Wahrscheinlichkeitsfunktion ist.
_Gesucht_: Modell parametrisiert über die Parameter $theta$, which induces $p_"model" (x;theta)$.
Wir berechnen:
$
  theta^* &= arg max_theta quad (p_"model"(X; 0))\
    &= arg max_theta quad product_i (p_"model"(i; theta)) quad | log(dot)\
    &= arg max_theta quad sum^m_(i=1) log(p_"model", theta) quad | div m\
    &= arg max_theta quad EE_(x tilde p_"data")[x]\
    &-> "Dies entspricht der Maximierung der Ähnlichkeit von" p_"data" "und" p^(theta)_"model"
$

