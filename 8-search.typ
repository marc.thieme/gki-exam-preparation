#import "lib.typ": *

= Suche
#definition("Begriffe")[
/ Agent:
  + rational & autonom
  + -> *Input* -> *Aktion* -> *Input* -> ...
  + hat *Ziel* und *Kosten*
  + _später_: Hat Nutzen und Belohnungen
  + Reflexagent $<->$ planender Agent
/ Umgebung:
  + *Vollständig* / *teilweise beobachtbar* $->$ *Einnerung* / *Speicher* wird benötigt
  + *Diskret* / *kontinuierlich*
  + *Determnistisch* / *Stochastisch*
  + *Einzelagent* / *Multliagent*
]

#definition("State-Space-Graph")[
  Graph von (ggf. abstrahierten) Umgebungszuständen und deren Übergängen.
]
#definition("Suchbaum")[
  Knoten zeigen Zustände, ensprechen aber egtl. Pfäden zu den Zuständen.
]
#definition("Fringe")[
  Prioritätenliste der entdeckten Knoten.
]

#proposition[A\*][
  Führe Dijkstra aus mit modifizierter Kostenfunktion: $overline(c) (u v) = h(v) + c(u v) - h(u)$\
  Initialisiere die Fringe mit $(S, h(S))$
]
