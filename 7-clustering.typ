#import "lib.typ": *

= Lecture 7 | Expectation Maximization & co.
== Clustering
#definition("Properties of a Distance Mesaure D")[
/ Symmetry: $D(A, B) = D(B, A)$
/ Constancy of Self-Similariy: $D(A, A) = 0$
/ Positivity (Separation): $D(A, B) = 0$ iff $A = B$
/ Triangle Inequality: $D(A, B) <= D(A, C) + D(B, C)$
]

#remark[
  Don't forget to normalize your distance measure (e.g. euclid. distance)
  if the dimensions have different units!\
  For example, weight & length.
]

#definition("Sum Squared Error")[
$
  "SSD"(C; D) = sum^n_(i=1) d(x_i, c(x_i))^2
$
]

#definition("K-Means")[
  + Initialize centroids randomly
  + Assign all points to nearest centroid
  + Re-center all centroids to be central w.r.t. their points
  + repeat
]
#theorem("K-Means converges")[
  K-Means coverges, but only *locally*.
   It has no approximation guarantee/ can be arbitrarily bad.\
  Since it does not converge globally, we need _sophistication_
  when choosing the start.
]
#proof[
  With each step, the SSD decrease or stays the same.
  Also, there are only finite possible arrangements of the centroids.\
  The clustering problem is NP-Hard, therefore it only converges locally.
]

For *k-Means++*, use furthest first initialization:
+ Pick a random data-point as center
+ Calculate the clusters (the associated cluster center of each point)
+ ... For the next colors, choose the next cluster centers each furthest 
away from all other cluster centers. Repeat from step 2.
-> improves approximation ratio guarantee to $O(log k)$ in expectation (
  over the randomness of the algorithm
) (k is the \# of clusters).

== Gaussian Mixture Models
#definition("Mixture Model")[
$
p(x) = sum^K_ (k=1) underbrace(p(k), "mixture coefficient") 
overbrace(p(x | k), "k-th mixture component")
$
]
#definition("Mixture of Gaussians")[
$
p(x) = sum^K_(k=1) pi_k N(x | mu_k, Sigma_k)
$
where $p(k) = pi_k$ and $sum_k (pi_k) = 1$
]
#remark("Parameters of MoG")[
  The parameters of a MoG are $theta = {pi_1, mu_1, Sigma_1, ..., pi_K, mu_K, Sigma_K}$
]
#theorem[
  A mixture of gaussian with infinite parameters can approximate any prob. distribution.
]

== Expectation Maximization
#definition("Responsibilities")[
  The probability of any data point belonging to a cluster $k_i$
]
#theorem("Responsibilities")[
(Bayes rule)
  $
    q_(i,k) = p(k_i = k | x_i) &= (p(x_i | k) p(k))/(sum_j p(x_i | j) p(j)) \
    &= (pi_k N(x_i | mu_k, Sigma_k)) / (sum^K_(j=1) pi_j N(x_i | mu_j, Sigma_j)) in (0, 1)
  $
]
#theorem("If we know which component generated which sample")[
#let qik = $q_(i, k)$
*Maximum Likelihood Esimate:*\
Let $qik in {0, 1}$ 1 iff the i-th sample was generated by the k-th mixture component.
  - Coefficients: $
pi_k = (sum_i qik) / N
$
  - Means | Just the center: $
mu_k = (sum_i qik x_i) / (sum_i qik)
$
  - Covariances: $
Sigma_k = (sum_i qik (x_i - mu_k)(x_i-mu_k)^T)/(sum_i qik)
$
]

== Latent Variable Models
#definition("Latent Variable")[
  Juxtaposed to observed variables. "Hidden" variables so to speak.
  In our model, there are hidden variables for each data point.
]

Our parametric model is $p_theta (x, z)$ where z is the random variable of the latent variables.

#definition("Marginal Distribution")[
$
  p_theta (x) = sum_z p(x, z)
$
]
Given our model, the propability of our sample is
$
  p_theta (x) = product^N_(i=1) p(x_i) = product^N_(i=1) lr((sum_z p(x_i | z)))
$
Because of the sum, the log-likelihood $L(theta)$ is hard to optimize:
$
  L(theta) = sum^N_(i=1) log lr((sum_z_i p(x_i | z)))
$
#let qdistr = $q_i (z_i)$
To fix this, we introduce an (arbitrary) new distribution(s) qdistr and do some mathmatical trickery.
As far as my interpretation goes, this variational distribution fixes the hidden variables/their probabilities
so we can account for them in our calculations.
$
  L(theta) &= sum^N_(i=1) log lr((sum_z_i qdistr p(x_i | z)/qdistr))\
  &= sum^N_(i=1) log lr((EE_qdistr p(x_i | z)/qdistr))\
("Jensen's") quad&>= sum^N_(i=1) EE_qdistr lr([log(p(x_i | z)/qdistr)]) =: cal(L)(q, theta)\
("simplify") &= sum^N_(i=1) EE_qdistr lr([log(p(x_i | z))]) - underbrace(EE_qdistr lr([qdistr]), #[constant w.r.t $theta$])
$
#remark("Optimization algorithm")[
  + *E* xpectation Step:
    - Fix parameters $theta = theta_"old"$
    - Maximize lower bounds w.r.t. q: $q_"new" = arg max_q cal(L)(q, theta_"old")$
  + *M* aximization Step:
    - Fix variational distribution $q = q_"old"$
    - Maximize lower bounds w.r.t. theta: $theta_"new" = arg max_theta cal(L)(theta, q_"old")$
  - $q_"new"(z_i) = p_theta_"old" (z_i | x_i) = (p_theta_"old" (x_i, z_i)) / (p_theta_"old" (x_i))$
]
Note again, that #qdistr can be chosen arbitrarily.
#proposition("Application to Gaussian Models")[
- Latent variables: Which mixture component $k$ produced which point are the hidden variables
$->$ this induces the variational distribution #qdistr. This dictates the probability for each point i
to have originated from the $k$-th mixture component. $#qdistr = p(k = k_i | x_i)$
- The parameters $theta$ define the mixture components
]
