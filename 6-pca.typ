#import "lib.typ": *

= Lecture 6 | PCA
== Dimensionality Reduction
== Statistik
Seien $X_i, X_j$ Feature Subsets im Datensatz (also beide gleich groß, jeweils
1D)
#definition(
  "Varianz (Vektor)",
)[
  $
    "Var"(X_i) = "Cov"(X_i, X_i) = sum_(x in X) lr(|x - overline(x)|)^2 / (|X|)
  $
]

#definition(
  "Covarianz (Matrix)",
)[
  $
    "Cov"(X_i, X_j) = sum_(x in X, y in Y) ((x - overline(x))(y - overline(y))) / (|X||Y|)
  $
  Für die Covarianzmatrix $Sigma$:
  $
    Sigma_(i j) = "Cov"(X_i, X_j)
  $
]

Reduziert Dimension des Feature Space
#definition(
  "Projection of a vector",
)[
  Project from one (higher dimensional) feature space onto another (lower
  dimensional feature space)
]
#remark("Good Reduction")[
  A dimensionality reduction is good when
  - Covariance after the projection is high
  - Training works well still
  - Projected Space has much lower dimension
]
#definition("Mean Squared Reproduction Error")[
  Reduction Projection $phi(v)$.\
  Mean Squared Reproduction Error: $
    E(u_1) = |v - (phi^(-1) circle.small phi) (v)|
  $
]
#remark[
  We can also write the projection in terms of a linear basis ${u_1, ..., u_M}$:
  $
    x = underbrace(sum^M_(i=1) z_i u_i, "projected") + underbrace(sum^D_(j=M+1) z_j u_j, "skipped")
  $
]
#theorem("MSRE maximizes Variance")[
  For any $n$-dimensional feature reduction, the MSRE is minimal for the
  projection which maximizes the variance in each dimension
]
#definition(
  "Principal Component",
)[
  The principal components are the directions in which the variances are maximal.
]

