#import "lib.typ": *

#let title = "Grundlagen der künstlichen Intelligenz"
#let professor = "Neumann & Pascal"
#let time = "WS 23/24"

#show: project.with(title, professor, time)
#set text(8pt)

#include("2-linear-regression.typ")
#include("3-model.typ")
#include("3-classification-log-regression.typ")
#include("5-training-process.typ")
#include("6-pca.typ")
#include("7-clustering.typ")
#include("8-search.typ")
#include("12-bayes-models.typ")
#include("grundlagen.typ")
#include("questions.typ")
#include("todo.typ")

= Learn by heart
@linear-regression-formula
#theorem(
  "Linear Regression Formula",
  arg: red,
)[
  $w^* = (X^T X)^(-1) X^T y$
]
