#import "lib.typ": *

= Lecture 14 | Reinforcement Learning
#remark[Components of a RL Agent][
  *A RL agent consists of at least one of the following components:*
  1. *Policy*: Behaviour of the Agent
  2. *Value-Function*: Evaluation-function for states and actions
  3. *Model:* Agent's representation of the environment
]

