#import "lib.typ": *

= Lecture 5 | Generalisierung / Hyperparametrisierung / Regularisierung / Ensamble
#proposition("Generalisierung verbessern")[
  Verbessere die *Generalisierung* des Modells durch Anpassen der *Kapazität* des Modells
]
#image("res/underfitting-overfitting.png", width: 40%)

== Regularisierung
- L1/L2 Regularizers
- Gleichsetzen von Parameters
#definition("Data Augmentation")[
  Invarianzen Erkennen. Zb. Label von Bildern invariant gegenüber Drehung/Spiegelung.
]

== Ensemble
#lemma[
  Das Mittel mehrerer (unabhängiger) Modelle ist leicht besser als jedes einzelne Modell.
]

#definition("Bagging (Bootstrap Aggregation)")[
  _Paralleles Training_ von _unabhängigen modellen_ auf _zufälligen Subsets_ der Daten
]
#proposition("Dropout Bagging")[
  Für jeden Minibatch:
  1. zufällige binäre Maske für alle Neuronen
  2. Update nur die Gewichte der nicht-maskierten Neuronen
  -> Bei Vorhersage keine Maske und Skalierung der Gewichte
]
#definition("Boosting")[
  _Sequentielles Trainieren_ von Modellen 
  mit Fokus auf bisher _falsch vorhergesagte Trainingspunkte_.
]

