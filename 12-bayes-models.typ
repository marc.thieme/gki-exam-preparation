#import "lib.typ": *

= Lecture 12 | Bayes Models
== Foward-Backward algorithm

#notation[
  #table(columns: 2, $z_t$, "States", $y_t$, "Observations")
]
#theorem[Bayes Rule][
  $
    underbrace(p(z|y,x), "Posterior") = (overbrace(p(y|z,x), "Likelihood")overbrace(p(z|x), "Prior")) / underbrace(p(y|x), "Evidence")
  $
]
#definition[Hidden Markov Model Concepts][
  #table(
    columns: 3,
    $b_t (z_t) = p(z_t|y_(0:t))$,
    [Belief],
    [A guess towards one state],
    $forall_(i, j) quad p(z_i|z_j)$,
    [Transition Probability],
    [From state $z_i$ to $z_j$],
    $forall_(i,j) quad p(y_i|z_j)$,
    [Emission Probability],
    [Prob. to oberserve $y_i$ given $z_j$],
    $forall_(t,k) quad alpha_t (z_(t,k)) = p(z_(t,k), y_(1:t))$,
    [Message],
    [Unnormalized Belief],
    $forall_(t,k) quad beta_t (z_(t,k)) = p(y_(t+1:T)|z_(t,k))$,
    [Backward message],
    [Reverse prediction update],
    $forall_(t,k) quad b_t (z_(t,k)) = (alpha_t (z_(t,k))beta_t (z_(t,k))) / (sum_i alpha_t (z_(t,i)) beta_t (z_(t,i)))$,
    [Smoothed belief],
  )
]

#theorem[HMM DP-Formulation][
  For time-step $t$. $z_(t,k)$ denotes the $k$-th state at the $t$-th timestep:
  $
    forall_k quad alpha_t (z_(t,k)) &= overbrace(p(y_t|z_(t,k)), "Emission Probability")

    sum_i overbrace(p(z_(t,k)|z_(t-1,i)), "transition probability") dot alpha_(t-1)(z_(t-1,i)) \

    forall_k quad beta_t (z_(t,k))  &= sum_i p(y_(t+1)|z_(t,i)) p(z_(t+1,i)|z_(t,k)) dot beta_(t+1) (z_(t+1,i))
  $
]
#remark[Normalize the end belief/the last message][
  $
    forall_i quad b_"final" (z_i) = (alpha_"final" (z_i)) / (sum_j (alpha_"final" (z_j)))
  $
]

