#import "lib.typ": *

#let ws22 = [WS22]
#let ss23 = [SS23]

= What I didn't know but needed in the past exams
#proposition[Hyper-Parameter Tuning][#ws22][
/ Manuelle Optimierung: Iterativ und Intuitiv ausprobieren
/ Grid Search: Alle Kombinationen Ausprobieren
/ Random Search: Zufälliges ausprobieren
/ Bayes'sche Optimierung: In jedem Schritt wird Informationsgehalt möglicher weiterer Experimente quantifiziert
]

#theorem[Eigenschaften von Suchalgorithme][#ss23][
/ Vollständig: Findet garantiert _eine_ Lösung
/ Optimal: Findet garantiert die _optimale_ Lösung
]

#theorem[Bayes Rule][#ss23][
  $
    underbrace(p(z|y,x), "Posterior") = (overbrace(p(y|z,x), "Likelihood")overbrace(p(z|x), "Prior")) / underbrace(p(y|x), "Evidence")
  $
]

== HMM
#definition[Filtering][
  The task of tracking $b_t (z_t) = p(z_t | y_(0:t))$ over time.
]
#definition[Smoothing][
Smoothing multiplies the forward and backward messages and normalizes them.
$
forall_(t,k) quad b_t (z_(t,k)) = (alpha_t (z_(t,k))beta_t (z_(t,k))) / (sum_i alpha_t (z_(t,i)) beta_t (z_(t,i)))
$,
]

== MDP
#definition[Optimal Quantities][
/ state: s
/ q-state: (s, a)
/ transition: (s, a, a')
]

#proposition[Value Iteration Algorithm][
  $
V_k^*(s) = max_a lr((r(s, a) + gamma sum_s' p(s' | s, a) V^*_(k-1) (s')))
$
]
#proposition[Policy Iteration Algorithm][
  $
Q_k^*(s, a) = r(s, a) + gamma sum_s' lr((p(s' | s,a) max_a' Q_(k-1)^* (s', a')))
$
]

