#import "lib.typ": *

= Lecture 2 | Linear Regression
== Normal Regression
_Assume that all noise is gaussian noise_ $y = f(x) + epsilon, quad epsilon tilde N(0, 1)$

#definition("Summed (/mean) squared error")[
  $"SSE" = sum^N_(i=1) (y_i - f(x_i))^2$
]
#remark(
  "Why squared error?",
)[
  - Fully differentiable
  - Easy to optimize
  - $F^*(x) = arg min_(f(x)) "SSE" => F^*(x) = EE[y|x]$ -> We always estimate the
    mean of the target function
]
#theorem(
  "With matrices",
)[
  $"SSE" = (y - X w)^T (y - X w)$\
  where $X = mat(1, x^T_1;1, x^T_2;dots.v, dots.v;1, x^T_n)$, $quad w = vec(w_0, dots.v, w_D)$, $quad y = vec(hat(y)_1, dots.v, hat(y)_n)$
]
How do we minimize the SSE? Solving the derivative yields:
#theorem("Linear Regression Formula", arg: red)[
  $w^* = (X^T X)^(-1) X^T y$
] <linear-regression-formula>

== General Linear Regression
Now, we add a first transformation before our linear regression
#proposition("Model")[
  $f(x) = Phi(x)^T w$
]

This model is still linear in $w$ though!

#theorem("Generalized Formula")[
  $w^* = (Phi^T Phi) ^ (-1) Phi^T y$\
  where $Phi = vec(Phi^T_1, dots.v, Phi^T_n)$ comprises basis vectores
] <general-linear-regression-formula>
#corollary[
  We can fit any non-linear function like this if we would know suitable basis
  vectors
]

== Ridge Regression
The error now comprises a *data term* and a *regularization term*:
$
  E_D (w) + lambda E_W (w)
$

#definition("Weight decay / Ridge Regression")[
  $L_"ridge" = (y - Phi w)^T (y - Phi w) + lambda w^T w$
]
#corollary("Ridge Regression Formular")[
  $w^*_"ridge" = (Phi^T Phi^T + lambda II)^(-1)Phi^T y$
]

