#import "lib.typ": *

= Lecture 3 | Klassifikation, logistische Regression
/ Regression: Kontinuierlicher Output
/ Klassifikation: Diskreter Output
/ Generatives Modell: Lerne p(x | c)
/ Diskriminatives Modell: Lerne p(c | x)

In der Klassifikation:
#definition("Linear Separierbar")[
  Klassen können durch eine Gerade getrennt werden
]
#definition("Linearer Klassifikator")[
$
  f(x) = w^T x + b lt.gt 0
$
-> Formel berechnen dot-product, aka. Abstand (und vor allem Orientierung!) zur Geraden.
]

#proposition("Modell für lineares Klassifizieren")[
$
  p(c = 1 | x) = sigma(w^T x + b)\
  p(c = 0 | x) = 1 - sigma(w^T x + b)

$
Also:
$
  p(c | x) &= p(c=1 | x)^c p(c=0 | x)^(1-c) = sigma(w^T x + b)^c (1 - sigma(w^T x + b))^(1-c)
$
("Exponential-Trick", um beide Werte zu einer Wahrscheinlichkeitsfunktion zu 'interpolieren'). 
Sample Note that the sigmoid comes up to avoid exploding anomalies.
]
#corollary("Umformung der Formel")[
$
  arg max_tilde(w) "lok lik"(tilde(w), D) = arg max_tilde(w) sum_i c_i log sigma(tilde(w)^T tilde(x)_i) + (1 - c_i)log(1 - sigma(tilde(w)^T tilde(x)_i))
$
where $tilde(x)_i = [1, x_(i,0), 1, x_(i,1), ..., 1, x_(i,n)]$ and $tilde(w) = [b, w_0, w_1, ..., w_n]$ are extended to include the bias.
Optimize with Gradient Descent
]

#remark[
  Es kann wie bei linearer Regression $w_T x -> Phi(x) = [Phi_1(x), Phi_2(x), ..., Phi_3(x)]$ ausgetauscht werden, um
  ein Dataset auf ein linear separables Dataset zu projezieren.
]

== Multiklassen Klassifizierung
*Softmax Funktion:*
$
  p(c = i | x) = exp(w^T_i Phi(x))/(sum^K_k=1 exp(w^T_k Phi(x)))
$
*Eklärung*
- Ein Gewichtsvektor pro Klasse
- Klassenwahrscheinlichkeit skaliert mit $w^T_i Phi(x)$
- Wahrscheinlichkeit -> Normalisierung notwendig
- Für zwei Klassen wäre ein zweiter Gewichtsvektor redundant
  - Optimierung analog zu logistischer Regression (mit Exponential-Trick)

#include("4-neural-networks.typ")

