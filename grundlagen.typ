#import "lib.typ": *

= Grundlagen
== Statistik
=== Varianz / Kovarianz
#theorem("Verschiebungssatz")[
  $E[X^2] = "Var"(X) + EE[X]^2$
] <verschiebungssatz>

Seien $X_i, X_j$ Feature Subsets im Datensatz (also beide gleich groß, jeweils
1D)
#definition("Varianz (Stochastik)")[
  $
    "Var"[X] = EE[lr((X - EE[X]))^2]
  $
]
#definition(
  "Varianz (Vektor)",
)[
  $
    "Var"(X_i) = "Cov"(X_i, X_i) = sum_(x in X) lr(|x - overline(x)|)^2 / (|X|)
  $
] <variance>

#definition(
  "Covarianz (Matrix)",
)[
  $
    "Cov"(X_i, X_j) = sum_(x in X, y in Y) ((x - overline(x))(y - overline(y))) / (|X||Y|)
  $
  Für die Covarianzmatrix $Sigma$:
  $
    Sigma_(i j) = "Cov"(X_i, X_j)
  $
] <covariance>

=== Bias / Expected Error
Sei $hat(f)(x)$ eine Funktion, die die Verlustfunktion $L=(y - hat(f)(x)^2)$ minimiert.
#definition("Bias eines Schätzers")[
  $"Bias"[hat(f)(x)] = EE[hat(f)(x) - f(x)]$
]

#theorem("Expected Error")[
  $EE[lr((f(x) - hat(f)(x)))^2] = "Bias"[hat(f)(x)]^2 + "Var"[hat(f)(x)] + "Var"[f(x)]$
]

